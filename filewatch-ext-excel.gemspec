Gem::Specification.new do |spec|
  files = []
  dirs = %w{lib}
  dirs.each do |dir|
    files += Dir["#{dir}/**/*"]
  end

  spec.name = "filewatch-ext-excel"
  spec.version = "0.2.0"
  spec.summary = "filewatch extensions for excel file input"
  spec.description = "Extensions for ruby-filewatcher, like xls, xlsx"
  spec.files = files
  spec.require_paths << "lib"

  spec.authors = ["Signify"]
  spec.email = ["dietmar@signifydata.com"]
  spec.homepage = "https://bitbucket.org/signify/ruby-filewatch-ext-excel"
  spec.license = 'MIT'

  spec.add_runtime_dependency "filewatch", '~> 0.6', '>= 0.6.5'
  spec.add_runtime_dependency "simple_xlsx_reader", '~> 1.0', '>= 1.0.2'
  spec.add_runtime_dependency "spreadsheet", '~> 1.0', '>= 1.0.4'
end

