require "filewatch/watch"
if RbConfig::CONFIG['host_os'] =~ /mswin|mingw|cygwin/
  require "filewatch/winhelper"
end
require "logger"
require "rbconfig"

include Java if defined? JRUBY_VERSION
require "JRubyFileExtension.jar" if defined? JRUBY_VERSION

module FileWatch::Ext
  class TailBase
    # how often (in seconds) we @logger.warn a failed file open, per path.
    OPEN_WARN_INTERVAL = ENV["FILEWATCH_OPEN_WARN_INTERVAL"] ?
                         ENV["FILEWATCH_OPEN_WARN_INTERVAL"].to_i : 300

    attr_accessor :logger

    class NoSinceDBPathGiven < StandardError; end

    public
    def initialize(opts={})
      @iswindows = ((RbConfig::CONFIG['host_os'] =~ /mswin|mingw|cygwin/) != nil)

      if opts[:logger]
        @logger = opts[:logger]
      else
        @logger = Logger.new(STDERR)
        @logger.level = Logger::INFO
      end
      @files = {}
      @lastwarn = Hash.new { |h, k| h[k] = 0 }
      @watch = FileWatch::Watch.new
      @watch.logger = @logger
      @sincedb = {}
      @sincedb_last_write = 0
      @statcache = {}
      @opts = {
        :sincedb_write_interval => 10,
        :stat_interval => 1,
        :discover_interval => 5,
        :exclude => [],
        :start_new_files_at => :end,
        :progressdb => false,
        :eof_close => false,
      }.merge(opts)
      if !@opts.include?(:sincedb_path)
        @opts[:sincedb_path] = File.join(ENV["HOME"], ".sincedb") if ENV.include?("HOME")
        @opts[:sincedb_path] = ENV["SINCEDB_PATH"] if ENV.include?("SINCEDB_PATH")
      end
      if !@opts.include?(:sincedb_path)
        raise NoSinceDBPathGiven.new("No HOME or SINCEDB_PATH set in environment. I need one of these set so I can keep track of the files I am following.")
      end
      @watch.exclude(@opts[:exclude])

      _sincedb_open
    end # def initialize

    public
    def logger=(logger)
      @logger = logger
      @watch.logger = logger
    end # def logger=

    public
    def tail(path)
      @watch.watch(path)
    end # def tail

    public
    def subscribe(&block)
      # to be overwritten
    end # def each

    protected
    def _open_file(path, event)
      # to be overwritten
    end # def _open_file

    protected
    def _read_file(path, &block)
      # to be overwritten
    end # def _read_file

    private
    def _close_file(path)
      @files[path].close
    end

    protected
    def _check_sincedb(eof, path, &block)
      now = Time.now.to_i
      delta = now - @sincedb_last_write
      if eof || delta >= @opts[:sincedb_write_interval]
        @logger.debug("writing sincedb (delta since last write = #{delta})") if @logger.debug?
        _sincedb_write
        _progressdb_write(path, eof, &block) if @opts[:progressdb]
        @sincedb_last_write = now
      end
    end

    public
    def sincedb_write(reason=nil)
      @logger.debug("caller requested sincedb write (#{reason})")
      _sincedb_write
    end

    protected
    def _sincedb_open
      path = @opts[:sincedb_path]
      begin
        db = File.open(path)
      rescue
        @logger.debug("_sincedb_open: #{path}: #{$!}")
        return
      end

      @logger.debug("_sincedb_open: reading from #{path}")
      db.each do |line|
        ino, dev_major, dev_minor, size, pos = line.split(" ", 5)
        inode = [ino, dev_major.to_i, dev_minor.to_i]
        @logger.debug("_sincedb_open: setting #{inode.inspect} to #{pos.to_i}")
        @sincedb[inode] = {:size => size.to_i, :pos => pos.to_i}
      end
    end # def _sincedb_open

    protected
    def _sincedb_write
      path = @opts[:sincedb_path]
      tmp = "#{path}.new"
      begin
        db = File.open(tmp, "w")
      rescue => e
        @logger.warn("_sincedb_write failed: #{tmp}: #{e}")
        return
      end

      @sincedb.each do |inode, meta|
        db.puts([inode, meta[:size], meta[:pos]].flatten.join(" "))
      end
      db.close

      begin
        File.rename(tmp, path)
      rescue => e
        @logger.warn("_sincedb_write rename/sync failed: #{tmp} -> #{path}: #{e}")
      end
    end # def _sincedb_write

    protected
    def _sincedb_delete(path)
      inode = @statcache[path]
      @sincedb.delete(inode)
      _sincedb_write
    end

    protected
    def _progressdb_write(path, eof, &block)
      if eof
        inode = @statcache[path]
        meta = @sincedb[@statcache[path]]
        line = [inode, meta[:size], meta[:pos]].flatten.join(" ")
        yield(path, line, :progressdb)
      else
        @statcache.each do |path, inode|
          meta = @sincedb[inode]
          if meta[:size] != meta[:pos]
            line = [inode, meta[:size], meta[:pos]].flatten.join(" ")
            yield(path, line, :progressdb)
          end
        end
      end
    end

    protected
    def _progressdb_delete(path, &block)
      inode = @statcache[path]
      meta = @sincedb[inode]

      line = [inode, meta[:size], meta[:pos]].flatten.join(" ")
      yield(path, line, :progressdb_del)
    end

    public
    def quit
      @watch.quit
    end # def quit
  end # class Tail
end # module FileWatch
